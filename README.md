# Graphql Server Boilerplate

graphql server written in typegraphql and typegoose

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installation

Use the package manager npm to install all packages.

```
npm install
```

### Prerequisites

Create an .env file on the project root and add.

```.dotenv

NODE_ENV=development
PORT=4000
DATABASE=<mongoDb URI>
ACCESS_TOKEN_SECRET=yourAccessTokenSecret
REFRESH_TOKEN_SECRET=yourRefreshTokenSecret

```

### Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Authors

* **Ronald Varghese** - *Initial work* 

## Acknowledgments

* Hat tip to anyone whose code was used
