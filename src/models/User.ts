import {getModelForClass, prop} from '@typegoose/typegoose'
import {Field, ObjectType} from 'type-graphql'

@ObjectType()
export class User {
    @Field()
    _id: string

    @Field()
    @prop({
        unique: true,
        required: [true, 'Please Provide the email']
    })
    email!: string

    @prop({required: [true, 'Please provide a password']})
    password!: string

    @Field()
    @prop({type: Number, default: 0})
    tokenVersion: number
}

export const UserModel = getModelForClass(User)

@ObjectType()
export class LoginResponse {

    @Field()
    accessToken: string
}