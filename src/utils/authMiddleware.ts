import {MyContext} from '../types/Context'
import {MiddlewareFn} from 'type-graphql'
import {verify} from 'jsonwebtoken'

export const isAuth: MiddlewareFn<MyContext> = ({context}, next) => {
    const authHeader = context.req.headers['authorization']

    if (!authHeader) {
        throw new Error('not Authenticated')
    }

    try {
        const token = authHeader.split(' ')[1]
        const payload: any = verify(token, process.env.ACCESS_TOKEN_SECRET!)
        context.payload = payload
    } catch (e) {
        throw new Error('not Authenticated')
    }

    return next()
}