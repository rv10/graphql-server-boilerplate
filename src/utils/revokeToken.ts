import {UserModel} from '../models/User'

export const revokeToken = async (userId: string) => {
    try {
        await UserModel.findByIdAndUpdate(userId, {$inc: {tokenVersion: 1}}, {new: true})
    } catch (e) {
        console.log(e)
    }
}