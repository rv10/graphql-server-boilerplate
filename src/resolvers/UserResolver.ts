import {Arg, Ctx, Mutation, Query, Resolver, UseMiddleware} from 'type-graphql'
import {compare, hash} from 'bcryptjs'
import {LoginResponse, User, UserModel} from '../models/User'
import {MyContext} from '../types/Context'
import {createAccessToken, createRefreshToken, sendRefreshToken} from '../utils/token'
import {isAuth} from '../utils/authMiddleware'

@Resolver()
export class UserResolver {
    @Query(() => String)
    @UseMiddleware(isAuth)
    hello(
        @Ctx() {payload}: MyContext
    ) {
        return `hi your user id is ${payload!.userId}`
    }

    @Query(() => [User])
    users() {
        return UserModel.find()
    }

    @Mutation(() => LoginResponse)
    async login(
        @Arg('email') email: string,
        @Arg('password') password: string,
        @Ctx() {res}: MyContext
    ): Promise<LoginResponse> {

        const user = await UserModel.findOne({email})
        if (!user) {
            throw new Error("Invalid username or Password")
        }

        const valid = await compare(password, user.password)
        if (!valid) {
            throw new Error("Invalid Password")
        }

        sendRefreshToken(res, createRefreshToken(user))

        return {
            accessToken: createAccessToken(user)
        }
    }

    @Mutation(() => Boolean)
    async register(
        @Arg('email') email: string,
        @Arg('password') password: string
    ): Promise<Boolean> {

        const hashedPassword = await hash(password, 12)
        try {
            await UserModel.create({
                email,
                password: hashedPassword
            })
        } catch (e) {
            return false
        }
        return true
    }
}