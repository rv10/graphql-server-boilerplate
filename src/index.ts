import 'reflect-metadata'
import 'dotenv/config'
import {ApolloServer} from 'apollo-server-express'
import Express from 'express'
import {buildSchema} from 'type-graphql'
import cors from 'cors'
import cookieParser from 'cookie-parser'
import {UserResolver} from './resolvers/UserResolver'
import {connect} from 'mongoose'
import {verify} from 'jsonwebtoken'
import {UserModel} from './models/User'
import {createAccessToken, sendRefreshToken} from './utils/token'


(async () => {

    await connect(process.env.DATABASE!, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })

    const app = Express()

    app.use(cors({
        credentials: true,
        origin: '*'
    }))

    app.use(cookieParser())


    app.get('/', (_, res) => res.send('Server under Maintenance'))

    app.post('/refresh_token', async (req, res) => {
        const token = req.cookies.jid
        if (!token) {
            return res.send({ok: false, accessToken: ''})
        }

        let payload: any = null
        try {
            payload = verify(token, process.env.REFRESH_TOKEN_SECRET!)
        } catch (e) {
            return res.send({ok: false, accessToken: ''})
        }

        const user = await UserModel.findById(payload.userId)
        if (!user) {
            return res.send({ok: false, accessToken: ''})
        }

        if (user.tokenVersion != payload.tokenVersion){
            return res.send({ok: false, accessToken: ''})
        }

        sendRefreshToken(res, createAccessToken(user))

        return res.send({ok: true, accessToken: createAccessToken(user)})
    })

    const apolloServer = new ApolloServer({
        schema: await buildSchema({
            resolvers: [UserResolver]
        }),
        context: ({req, res}: any) => ({req, res})
    })

    apolloServer.applyMiddleware({app})

    app.listen(process.env.PORT, () => {
        console.log(`Server Started on http://localhost:${process.env.PORT}/graphql`)
    })
})()